---
marp: true
paginate: true
autoscale: true
theme: uga
---
<!-- _paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)

# What a Context-Aware Classroom can do for you?
## Philippe Dessus, LaRAC & LIG, Univ. Grenoble Alpes
### M-PSI Seminar, LIG
July 13, 2023

---
<!-- footer: Philippe Dessus | LaRAC & LIG, UGA | What a CAC can do for you? | 07/12/23 | CC:BY-NC-SA -->
<!-- paginate: true -->

# Goal of this presentation

- **Why and how to study** Context-Aware Classrooms (a.k.a. CACs)?
- How researchers can use CACs to better understand **psychological and pedagogical processes** occurring in higher education classrooms settings?

---
# :information_source:

- URL of this presentation: https://link.infini.fr/cac-lig-23
![w:300](images/qr-cac-23.png)
- :books: **References** at the end of the presentation
- **Text in blue**: hypertext links to the internet


---
# :id: Who am I? 

- I'm a primary and secondary teachers trainer, and a researcher in educational sciences and technologies
- [Automated assessment of students' written production] +
[Classroom climate assessment] = 
**How to automatically assess classroom climate**?

---
# Outline

0. What is a CAC?
1. Theoretical accounts
2. Some psychological and pedagogical constructs
3. Discussion

---
# :zero: What is a Context-aware Classroom?


---
# 0.1 What is a CAC?

1. A **regular** classroom with sensors, controllers, actuators, and data analysis devices…
2. … to capture, gather and analyze teaching-learning events
3. … to help actors (teachers, students, researchers) make decisions

---
# 0.2 An overview of the messy literature about CACs

- A lot of **synonyms**: ambient, intelligent, adaptive, ubiquitous, responsive, smart, pervasive…
- A lot of **techniques** under the hood: viz, statistics, deep learning, …
- A lot of “solutionist“ stances, proposing stuff that don't adress any actual problem(Morozov 2013)
- Very few theretically grounded studies
- Mostly prototypical in Western countries (see however the [ALTSchool](https://en.wikipedia.org/wiki/AltSchool) company)

---
# 0.3 A functional classification of CACs (Laurent et al. 2021)

- **Tools** (actuator-centered): to trigger actions in place of teachers (e.g., turn on a device when a given condition is met)
- **Instruments** (sensor-centered): to capture, analyze, gather classroom-context related variables (e.g., student's attention) that are in turn displayed to actors (e.g., teachers, students) to solve **short terms** problems
- **Thermometers** (controller-centered): like an instrument, but mainly for research purposes (e.g., have a better view on students' engagement) and to solve **long term** problems

---
# :one: Theoretical accounts for studying CACs :bulb:

---
# 1.1 Which theories can be useful to study CACs?

1. **Ecological psychology**: Studying students and teachers in authentic settings
2. **Pedagogy-centered**: Validate pedagogy-centered theories (e.g., steering group, see Kounin 1970)
3. **Socially-oriented**: Teacher-students relationships (classroom climate, see Pianta et al. 2012)
4. **Critical studies of technology**: Analyze how CACs and datafication of education can fire back (e.g., ethical problems, surveillance, see Selwyin, in press)


---
# 1.2 Ecological psychology (1/2)

- Actors in a CAC are inserted in perception–action loops (capturing, processing information to make decisions or actions, etc.)…
- …helped by a series of artifacts (for action or decision) considered as part of the actors
- …sensitive to main dimensions of the educ. context: time, space, social, epistemic

---
# 1.2 Ecological psychology  (2/2)

| Features                                                                                                                                                                                                                                                                                                                                                                                                                                           | Coding                                                                                                                                   |
|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------|
| “The presentations are then “submitted” and the professor, and eventually other students if that is deemed desirable, are able to review and comment, with the comments anchored to specific points in the presentations.”| L3. Teacher: (P) Views students’ presentations ← [Presentation system] (A) Gives feedback} |


:books: Dessus et al. (in prep.); Winer & Cooperstock (2002)

---
# 1.3 Pedagogy-centered — The Superviseur Project (1/2)

- What **attentional distribution** patterns of teachers (novices vs. experts) in the classroom, in the most ecological situation possible?  
- 4 grade-1 & 2 **teachers**  (2 novices and 2 experts) carry out a mathematics session with a mobile eye-tracker
- Gathering **eye fixations** to students  

:books: Dessus et al* 2016; [http://superviseur.lip6.fr](http://superviseur.lip6.fr)

---
# 1.3 Pedagogy-centered — Superviseur (2/2)


![w:1000](images/superviseur.jpeg)


---
# 1.4 Socially-oriented: Classroom Climate

- The **Classroom Assessment Scoring System** (CLASS) is a validated human-based observation system centered on classroom climate (K-12)
- Observers gauge 10 dimensions in 3 domains (emotional, classroom management, learning support)
- Twofold research goal in our team: 1/ adapt  a system for higher education; 2/ design and validate an automated system

:books: Dessus, Tanguy & Tricot 2016; Pianta et al. 2012; Ramakrishnan et al. 2020

---
# 1.5 Critical studies of technology (1/2)

- Datafication, platformisation, and surveillance of education
- A “low achiever” can now be solely detected by data, without any other contextual explanations (e.g., sociological, human…)
- Classrooms have now “**eyes and ears**”, extending the boundaries of what can tracked and analyzed (e.g., gaze, emotions) and **there can be no safe-guards** to their further exploitation
- A manifesto describing 4 main safe-guard practices

:books: Laurent et al. (2020); Selwyn (in press)

---
# 1.4 Critical studies (2/2) Laurent et al. (2020)

1. [The CAC] **obfuscates** all participants, while focusing on meaningful events and constructs (gaze, teacher cognitive load, hand, deictics, gesture, non-personal devices, voice activity and overlap, prosody).
2. We promote a **global** rather than local methodology: students’ behaviours will never be individually traceable; the machine learning being limited to restore a globalised picture of the occurrence of behaviours. In other words, our data reports about the whole classroom but ignores individuals.
3. Machine learning will be intentionally constrained to a **delayed feedback**, filtered by the research team, excluding any real-time monitoring or ad hominem reports. 
4. [The CAC] **won't be used to make high-stakes decisions**, monitor staff performance or student insulation.

---
# :two: Psychological and pedagogical constructs to study in CACs 

---
# 2.1 Some psychological constructs (1/2)

- **Teacher attention**
  - Teacher's gaze distribution across students (both to get information and communicate)(Sümer et al. 2018)
- **Students' attention**
  - Attention (head direction) to learning material, to teacher, to peers (Zaletelj & Košir 2017)
- **Students' synchrony**
  - Clue of collaboration and shared engagement (Goldberg et al. 2021)
- **Teacher-Students synchrony**
  - Clue of behavioral engagement (Goldberg et al. 2021)
- **Teacher's gestures**
  - Pointing to students, material, rythming teacher's speech (Barmaki 2016)

---
# 2.1 Some psychological constructs (2/2)

- **Teachers' cognitive load**
  - Pupil dilation (Prieto et al. 2015)
- **Emotional cues**
  - Clue of classroom climate quality (Zhou et al. 2022)
- **Students' engagement**
  - Whether they are on/off topic, internal (thinking)/external(acting) (Keller et al. 2020)

---
# 2.2 Some pedagogical constructs

- **Speech distribution** within instructional episodes (Owens et al. 2017)
- **Students' engagement** as a function of teacher's location (Lermigeaux-Sarrade 2018)
- Effect of **table location** on teacher's supervision (Lermigeaux-Sarrade 2018)
- **Steering groups** (Dessus et al. 2016)
- Imitation, **shared attention** during instruction (Gergely & Csibra 2006)
- Effect of **cultural settings** (McIntyre et al. 2020)

---
# :three: Discussion :thread:

---
# 3. Discussion

- CACs are versatile tools for educational research that can do a lot for you…
- …gathering and analyzing multimodal data…
- …about a large range of psychological and pedagogical processes in a (rather) unobtrusive way
- **BUT**
- :warning: CACs can help surveillance
- :warning: CACs hinder teacher's cognitive load
- :warning: CACs deeply change the classroom's ecology
- **THEN (as a take-away)**
- Computer processing power has to be used to obfuscate data, and empower learners and teachers, **not to harm their agentivity and privacy**

---
# That's it! Thanks for your attention! :pray:

- :interrobang: Any questions :interrobang:
- :e-mail: philippe.dessus[at]univ-grenoble-alpes.fr

---
# References (1/3)

<!-- _class: t-50 -->

- Barmaki, R. (2016). Gesture Assessment of Teachers in an Immersive Rehearsal Environment University of Central Florida. Orlando. 
- Dessus, P., et al. (in prep.). Context-Aware Classrooms : An Ecological Psychology Framework.
- Dessus, P., Cosnefroy, O., & Luengo, V. (2016). “Keep your eyes on ‘em all!”: A mobile eye-tracking analysis of teachers' sensitivity to students. In K. Verbert, M. Sharples, & T. Klobučar (Eds.), Adaptive and adaptable learning. *Proc. 11th European Conf. on Technology Enhanced Learning (EC-TEL 2016)* (pp. 72–84). Springer. 
- Dessus, P., Tanguy, F., & Tricot, A. (2015). Natural cognitive foundations of teacher knowledge: An evolutionary and cognitive load account. In M. Grangeat (Ed.), *Understanding science teacher professional knowledge growth* (pp. 187–202). Sense Publishers. 
- Gergely, G., & Csibra, G. (2006). Sylvia's recipe: The role of imitation and pedagogy in the transmission of cultural knowledge. In N. J. Enfield & S. C. Levenson (Eds.), *Roots of human sociality: Culture, cognition, and human interaction* (pp. 229–255). Berg. 
- Goldberg, P., Sümer, Ö., Stürmer, K., Wagner, W., Göllner, R., Gerjets, P., Kasneci, E., & Trautwein, U. (2021). Attentive or Not? Toward a Machine Learning Approach to Assessing Students’ Visible Engagement in Classroom Instruction. *Educational Psychology Review, 33*(1), 27–49. https://doi.org/10.1007/s10648-019-09514-z
- Keller, A. S., Davidesco, I., & Tanner, K. D. (2020). Attention Matters: How Orchestrating Attention May Relate to Classroom Learning. *CBE Life Sci Educ, 19*(3), fe5. https://doi.org/10.1187/cbe.20-05-0106 
- Kounin, J. S. (1970). *Discipline and group management in classrooms*. Holt, Rinehart & Winston. 
- Laurent, R., Vaufreydaz, D., & Dessus, P. (2020). Ethical teaching analytics in a Context-Aware Classroom: A manifesto. *ERCIM News*, *120*, 39–40. 
- Laurent, R., Dessus, P., & Vaufreydaz, D. (2021). Apprendre en toute éthique dans les salles de classe intelligentes. *Que dit la recherche*. Poitiers : Canopé, Agence des usages.

---
# References (2/3)

<!-- _class: t-50 -->

- Lermigeaux-Sarrade, I. (2018). Rôle de l'organisation de l'espace de travail dans les activités effectives et empêchées des enseignants [Role of work space organization in impeded or actual teacher activities] [PhD Thesis in education, Université Grenoble Alpes]. Grenoble. 
- McIntyre, N. A., Mulder, K. T., & Mainhard, M. T. (2020). Looking to relate: teacher gaze and culture in student-rated teacher interpersonal behaviour. *Social Psychology of Education*, *23*(2), 411-431. https://doi.org/10.1007/s11218-019-09541-2 
- Morozov, E. (2013). *To save everything, click here: Technology, solutionism, and the urge to fix problems that don't exist.* Lane.
- Owens, M. T. et al. (2017). Classroom sound can be used to classify teaching practices in college science courses. *Proc Natl Acad Sci USA, 114*(12), 3085-3090. https://doi.org/10.1073/pnas.1618693114 
- Pianta, R. C., Hamre, B. K., & Mintz, S. L. (2012). *Classroom Assessment Scoring System: Secondary manual*. Teachstone. 
- Prieto, L. P., Sharma, K., Wen, Y., & Dillenbourg, P. (2015). The burden of facilitating collaboration: Towards estimation of teacher orchestration load using eye-tracking measures. *11th Int. Conf. on CSCL 2015*, Gothenburg. 

---
# References (2/3)

<!-- _class: t-50 -->

- Ramakrishnan, A., Zylich, B., Ottmar, E., LoCasale-Crouch, J., & Whitehill, J. (2020). Toward Automated Classroom Observation: Multimodal Machine Learning to Estimate CLASS Positive Climate and Negative Climate. https://doi.org/arXiv:2005.09525 
- Selwyn, N. (in press). Critical data futures. In W. Housley, A. D. Edwards, R. Montagut, & R. Fitzgerald (Eds.), *The SAGE handbook of digital society*. SAGE. 
- Sümer, Ö., Goldberg, P., Stürmer, K., Seidel, T., Gerjets, P., Trautwein, U., & Kasneci, E. (2018). Teachers’ perception in the classroom. *Second Workshop on Computational Models Learning Systems and Educational Assessment (CMLA 2018), in conjunction with CVPR 2018*, Salt Lake City.
- Winer, L.R. and J. Cooperstock, J. (2002). The ‘‘Intelligent Classroom’’: changing teaching and learning with an evolving technological environment,”. *Computers & Education*, *38*, 253–266.
- Zaletelj, J., & Košir, A. (2017). Predicting students’ attention in the classroom from Kinect facial and body features. *EURASIP Journal on Image and Video Processing*, 2017(1). https://doi.org/10.1186/s13640-017-0228-8 
- Zhou, H., Jiang, F., Si, J., Xiong, L., & Lu, H. (2022). Stuart: Individualized classroom observation of students with automatic behavior recognition and tracking. ArXiv preprint. https://doi.org/arXiv:2211.03127v1 