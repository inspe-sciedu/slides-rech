---
marp: true
paginate: true
autoscale: true
theme: uga
---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)

## Observer et analyser la qualité des relations enseignant-élèves
### Philippe Dessus, LaRAC & LIG, UGA 
### Webinaire C.A.R.E.C., Rectorat de l'académie de Grenoble
### 21 mars 2023


---
<!-- paginate: true -->
<!-- footer: Ph. Dessus LaRAC-UGA | Webinaire CAREC, Rectorat Grenoble | CC:BY-NC-SA | 21/03/23 -->


<!--https://www.mdpi.com/1660-4601/20/3/2116 -->

# Présentation

- :globe_with_meridians: [Présentation en pdf](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides-rech/23-carec.pdf)
- :books: Références en fin de présentation ; les [références en bleu]() sont des liens hypertextes vers des pages d'internet

---
# :zero: Introduction : Évaluer les REE, une (fausse) bonne idée ?

---
## 0. Les REE ? qu'est-ce que c'est ?

Les relations enseignant-élèves est défini comme le climat socio-émotionnel de la classe, elles ont 3 caractéristiques (Pianta 1999, p. 72, voir aussi Laurent *et al.* 2022) :

- **idiosyncrasique**, dépendant des individus impliqués, leur histoire, leurs facteurs biologiques, etc.
- **interactionnelle**, processus d'échange, d'interactions, entre individus
- **asymétrique**, l'enseignant détermine les REE avec un poids plus grand, avec des influences internes et externes à leur relation

Ici, centration sur la 2e caractéristique, interactionnelle

---
## 0. L'enquête TALIS 2018 (OCDE) (1/2)

Enquête internationale TALIS de l'OCDE (*Teaching and Learning International Survey*) explore en 2018 les niveaux élémentaire et secondaire, 48 pays, 260 000 enseignants (données collège)

- 96 % des enseignants français estiment que le bien-être des élèves est important ; qu'ils s'entendent généralement bien avec les élèves (moyenne OCDE 96 %) (**climat positif**)
- 92 % des enseignants français estiment qu'ils s'intéressent à ce que les élèves ont à dire (moyenne OCDE 93 %) (**sensibilité**)
- 95 % des enseignants français disent qu'ils donnent de l'aide aux élèves qui en ont besoin (moyenne OCDE 92 %) (**rétroactions**)

:books: OCDE 2019

---
## O. L'enquête TALIS 2018 (OCDE) (2/2)

- 35 % des enseignants français disent qu'ils doivent attendre assez longtemps que les élèves se calment, au début de la séance (moyenne OCDE 28 %) (**gestion de la discipline**)
- 32 % des enseignants français disent qu'il y a beaucoup de bruit qui perturbe leur classe (moyenne OCDE 25 %) (**gestion de la discipline**)


---
## 0. Observer les relations pour les évaluer ?

- Observer *vs*. questionner les apprenants (élèves ou étudiants) ?
	- L'évaluation par les apprenants n'est **pas corrélée** avec leur apprentissage (passé ou futur)
- Questionner les enseignants ?
  - Biais de désirabilité : seulement 4 % des enseignants ne se disent pas soucieux du bien-être de leurs élèves
- Observer *vs*. inspecter ?
	- **Décrire** des situations pour mieux les **comprendre** *vs*. les **harmoniser**, les contrôler, en relation avec des directives

:books: Marcel 2017 ; Uttl *et al*. 2017

---
## 0. Relations enseignant-élèves (REE) ?

- Rendre compte du **processus** d'enseignement et pas de la qualité de l'enseignant en lui-même, ou des valeurs poursuivies
- Ce qui est observé est une **petite partie** des REE (voir plus loin)

:books: Donker 2021 ; Doussot 2018 ; Fauth *et al*. 2020

---
## 0. Mais attention aux effets indésirables !

- Vouloir tout fonder sur les preuves pose certains problèmes 
  - On n'agit que sur ce qui est **mesurable**, et l'on peut passer à côté d'éléments importants mais moins aisément mesurables (Biesta 2020)
  - On ne s'intéresse pas aux **effets indésirables** : on peut avoir des élèves performants mais qui ont perdu de l'intérêt ou de la motivation (Zhao 2017)

---
## 0.  Comment observer ? (exemples)

Systèmes d'observation :
	- bas niveau d'inférence généraliste : *Teaching Dimensions Observation Protocol* (TDOP, Hora *et al.* 2013)
	- haut niveau d'inférence généraliste : *Classroom Assessment Scoring System* (CLASS, Pianta *et al*. 2008)
	- haut niveau d'inférence spécialisé : *Individualizing Student Instruction* (ISI, Connor *et al.* 2011)


---
## 0. Observer les REE : plan

1\. Dimensions de la qualité des REE ?
2\. Observer et mesurer la qualité des REE :arrow_right: Présentation sommaire du CLASS (domaine socio-émo.)
3\. Les REE au CP, quelques recherches
4\. La part du contexte dans les REE ? :arrow_right: Une étude franco-canadienne
5\. Une grande partie des REE est cachée ou difficilement traçable :arrow_right: Que nous dit l'analyse du regard ?
6\. Les REE dès les enseignants novices ? :arrow_right: Former les enseignants aux REE ?
7\. Discussion

:books: Biesta 2020 ; Dessus *et al.* 2005 ; Nuthall 2007 ; Rogalski 2003

<!--des actions non contiguës peuvent avoir des buts similaires, des actions contiguës peuvent avoir des buts différents-->

---
# :one: Dimensions de la qualité des REE

---
## 1. Trois dimensions (domaines) de base

- *la gestion de la classe* : optimiser le temps de l'enseignement
- *l'activation cognitive* : stimuler et aider l'apprentissage, la pensée
- *aide constructive à l'élève* : soutenir l'autonomie, l'autorégulation (**socio-émotionnel**)

Ces dimensions sont inter-reliées : améliorer le niveau d'une dimension améliore le niveau des autres

:books: Molina *et al.* 2018 ; Praetorius *et al.* 2018

---
## 1. L'importance des compétences socio-émo

**Prédicteur important** de la réussite des élèves, à tous niveaux. Pour un élève, et toutes choses égales par ailleurs, un point supplémentaire sur l’échelle de compétence socio-émo (évaluée par l'enseignant) :

- augmente de 54 % ses chances d’avoir le baccalauréat 
- double ses chances d’avoir un diplôme universitaire 
- augmente de 46 % ses chances d’avoir un travail à temps plein à 25 ans

:books: Jones *et al.* 2015 ; Schneider & Preckel 2017

---
## 1. Qualité des REE :arrow_right: réussite des élèves

1. Étude randomisée sur les effets de *My Teaching Partner* (MTP) sur 2 ans, fondé sur CLASS (*N* = 173) en **maternelle**. Effet sign. : développement socio-émo :arrow_right: engagement des élèves :arrow_right: perf. en littéracie
2. Étude randomisée sur les effets d'un atelier de développement professionnel (*N* = 54) en **maternelle** (programme proche du CLASS). Le socio-émo. a un effet sign. sur l'intérêt des élèves ; la gestion de classe et le soutien à l'appr. sur l'engagement
3. Étude randomisée sur les effets de MTP sur 1 an fondé sur CLASS (*N* = 78), **2nd degré**. Effet faible mais significatif de l'intervention sur les résultats des élèves

:books: 1. Pianta *et al*. 2022 ; 2. Fauth *et al.* 2019 ; 3.  Allen *et al*. 2011

---
# :two: Présentation sommaire du 
# *Classroom Assessment Scoring System*
# (Domaine Soutien émotionnel)
## Cf. annexe pour les autres domaines

---
## 2. Le rôle de CLASS
- CLASS est un outil d'observation de la qualité des interactions enseignant·e-élèves
- Conçu au début des années 2000 par l'équipe de Robert C. Pianta (univ. de Virginie)
- Utilisable de plusieurs manières
	- avoir un vocabulaire commun
	- évaluation de la qualité des REE (description)
	- se centrer sur des pistes d'analyse de situations et d'amélioration (prescription)
	
---
## 2. CLASS | Domaine 1 | Soutien émotionnel

Version présentée : Pre-K ou K3 (soit de la Petite section au CE2)
- Des relations chaleureuses, soutenantes avec les adultes et les autres enfants
- La joie et le désir d'apprendre
- Une motivation à s'engager dans les activités d'apprentissage
- Un sentiment de confort (aisance) dans le milieu éducatif
- La volonté de relever des défis sur le plan social et académique
- Un niveau approprié d'autonomie

---
## 2. Soutien émotionnel - 1. Climat positif (C+)
Reflète le lien émotionnel entre l'enseignant et l'enfant, entre les enfants eux-mêmes ainsi que la chaleur, le respect et le plaisir communiqué dans les interactions verbales et non-verbales

**Indicateurs**
- Relations
- Affect positif
- Communication positive 
- Respect

---
## 2. Soutien émotionnel - 2. Climat négatif (C–)
N'est pas simplement l'absence de climat positif mais bien la présence de comportements négatifs. Reflète l'ensemble de la négativité exprimée dans la classe. La fréquence, l'intensité et la nature de la négativité de l'enseignant et des enfants sont des éléments-clés de cette dimension

**Indicateurs**
- Affect négatif
- Contrôle punitif
- Sarcasme/Irrespect
- Sévère négativité

---
## 2. Soutien émotionnel - 3. Sensibilité de l'enseignant (SE)
Englobe la capacité de l'enseignant à être attentif·ve et à répondre aux besoins émotionnels et académiques des enfants. Un haut niveau de sensibilité offre un soutien aux enfants dans l'exploration et l'apprentissage puisque l'enseignant leur offre de manière constante du réconfort et de l'encouragement

**Indicateurs**
- Conscience/vigilance
- Réceptivité
- Réponse aux problèmes
- Confort des élèves

---
##  2. Soutien émotionnel - 4. Prise en considération du point de vue de l'enfant (PVE)
Permet de rendre compte dans quelle mesure les interactions de l'enseignant avec les enfants et les activités offertes mettent l'accent sur les intérêts des enfants, leur motivation et leur point de vue tout en encourageant la responsabilité de l'enfant et son autonomie

**Indicateurs**
- Souplesse et attention centrée sur l'élève
- Soutien à l'autonomie et au leadership
- Expression des élèves
- Restriction de mouvement

---
# :three: REE au CP 

---
## 3. Compilation de 4 études en CP

- Étude 1 : *N* = 40 - Liens entre **motivation en lecture et relations** enseignant-élèves (ADVENIR)
- Étude 2 : *N* = 35 - Développement des **habiletés en lecture et écriture** dans la scol. élémentaire (LONGIT)
- Études 3-4 : *N* =  resp. 135 & 140 - Effets des **CP à effectifs réduits** (CP12)

:books: 1. Cosnefroy *et al*. 2016 ; 2. Bressoux *et al*. 2016 ; 3-4 : Lima *et al*. 2020

---
## 3. Compilation de 4 études en CP

![w:1000](images/class.jpg)

---
## 3. Problèmes

- Beaucoup de variables sont “**dans la moyenne**”, et même les plus variables des dimensions concentrent les 3/4 de leurs relevés dans 3 catégories seulement
- Un accès à des améliorations est possible seulement si les événements menant à des catégories basses (et hautes) **sont clairement identifiés**

:books: Kelly *et al*. 2020

---
# :four: La part du contexte dans les REE ?

---
## 4. La part du contexte : étude

- Comparaison **franco-canadienne** de la qualité de l'accueil en école maternelle resp. Centres de la petite enfance
- Évaluation de la qualité des interactions avec l'**in-CLASS** et le **CLASS** sur 80 classes
- Résultat principal : la qualité des interactions est **significativement supérieure** dans les Centres québécois, pour presque toutes les dimensions du CLASS (sauf Dév. de concepts)
- Imputable aux enseignants ? au contexte scolaire ? à la culture ?

:books: Bigras *et al*. 2020 ; Downer *et al*. 2010 ;  Pianta *et al*. 2008

---
## 4. La part du contexte : résultats

![w:1000](images/class-quebec.jpg)

---
## 4. La part du contexte : interprétation

- Des items du CLASS ne sont pas seulement sous l'influence de l'enseignant, **mais aussi des élèves et de la structure**
- Certaines mesures de la composition des classes (% de garçons, niveau moyen, taille de la classe) sont significativement prédictrices des scores CLASS
- Les différences France-Québec peuvent être dues au différences d'enseignement, ou encore aux différences de contexte
- Les traditions pédagogiques d'un pays peuvent différer des prescriptions du CLASS (Evertsen 2023, montrant que la dimension de productivité n'est pas alignée avec les traditions norvégiennes)

:books: Kelly *et al*. 2020

---
## 4. Vers une analyse multimodale ?

- Recherche nécessitant un important travail d'analyse “à la main” (des événements réels aux codes), très coûteuse
- Des outils récents d'analyse et traitement du signal permettent de faire cette analyse automatiquement
- Mais problèmes éthiques importants : tout ce qui est capté peut être analysé

:books: [Edusense](https://www.edusense.io) ; Petrova *et al*. 2020

---
# :five: Que nous dit l'analyse du 👁️ ?

---
## 5. Analyse du regard et enseignement

- La direction et trajectoire du regard (de l'enseignant, des élèves) sont cruciaux
- Plusieurs rôles : **attentionnel** (quand on pose une question), **communicatif** (quand on parle)
- Étudiés depuis longtemps  ”**manuellement**”, depuis peu avec des moyens plus fiables
	- oculomètres fixes (attention sur des films vidéo) 
	- oculomètres mobiles (attention *in vivo*)

:books: Cortina *et al*. 2015 ; Kounin 1970 ; McIntyre *et al*. 2017 ; Wolff 2015 

---
## 5. Projet *Superviseur*

- Quelle **distribution attentionnelle** des enseignants (novices vs. experts) en classe, dans une situation la plus écologique possible ?  
- **4 enseignantes** de CP-CE2 (2 novices et 2 expérimentées) réalisent une séance de mathématiques avec oculomètre mobile
- Récupération des **fixations oculaires** sur les élèves 

:books: Dessus *et al*. 2016 ; [http://superviseur.lip6.fr](http://superviseur.lip6.fr)

---
## 5. Regarder les élèves

![w:1000](images/superviseur.jpeg)

---
# :six: Former aux REE ?

---
## 6. Former les enseignants aux REE

- Les REE sont difficiles à percevoir et à analyser...
- Mais on peut réfléchir à des simplifications opérationnelles
- Évaluation de fines tranches (*thin slices*) de 30 à 90 s de vidéos

:books: Babad 2007 ; Begrich *et al*. 2021

---
## 6. Les REE : apprentissage entre pairs

- Sensibilisation aux REE en 2 séances de 2 h en Inspé (mémoire Master 2)
- 2 ens. stagiaires de Master 1 (Inspé) se sont mutuellement observées en cours, en se centrant sur une dimension du domaine socio-émotionnel (CLASS)
	- Les commentaires importent plus que les scores
	- Une part intéressante des REE est notée, notamment les incidents, ensuite analysés
	- Des réattributions de catégories d'événements assez fréquents

:books: [Site sur la sensibilisation au Classroom Assessment Scoring System](http://espe-rtd-reflexpro.u-ga.fr/docs/sciedu-cours-class/fr/latest/)

---
# :seven: Discussion

---
## 7. Discussion (1/3) : Rôles de l'observation à base de CLASS

- Former les enseignants au socio-émo, notamment pour éviter l'abandon du métier (Pelletier 2022)
- **Vocabulaire commun** pour référer à et discuter des pratiques
- **Étudier** l'enseignement et l'apprentissage
- **Évaluer** l'enseignement et l'apprentissage
- Développement professionnel des enseignants (“*coaching*” ou supervision)

---
## 7. Discussion (2/3) : Problèmes

- Difficultés dans l'**évaluation** des REE : influence de la situation, de la culture, lecture des émotions
- Problèmes de **forte prescription** du socio-émo : le travail émotionnel des enseignants
- Problèmes du score : observer :arrow_right: juger la situation :arrow_right: réduire le jugement en un score :arrow_right: le contexte est perdu ; les mesures ne doivent pas décider des valeurs
- Rôle politique de la *Bill & Melinda Gates foundation* dans le projet MET (*Measures of effective teaching*)?

:books:  Barrett 2017 ; Hochschild 2017 ; Koschmann 2017 ; [Strauss 2011](https://www.washingtonpost.com/blogs/answer-sheet/post/imagine-a-500000-gates-foundation-grant-to-harvard/2011/08/29/gIQAREysqJ_blog.html)

---
## 7. Discussion (3/3) :  Prochains développements

- [Class-Card](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/gene/class-card.html), un jeu de plateau simulant les étapes de décision de l'enseignement, à base de CLASS
- [Ateliers profs-chercheurs](https://www.profschercheurs.org/fr), recherche collaborative ouverte pour relever des défis dans l'éducation
- [Teaching Lab](https://teachinglab.m-psi.fr), une salle de classe ambiante et intelligente capturant les REE de manière non intrusive et éthique pour le développement professionnel des enseignants


:books: [Ateliers profs-chercheurs](https://www.profschercheurs.org/fr) ; [Class-Card](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/gene/class-card.html) ; Dessus *et al*. 2020 ; Laurent *et al*. 2020 ; [Teaching Lab](https://teachinglab.m-psi.fr) ; [Projet TALISMAN](http://talisman.m-psi.fr) ;

---
# Merci de votre attention ! Des questions ?

:e-mail: philippe.dessus[at]univ-grenoble-alpes.fr

Les études présentées ici ont été en partie financées par la DEPP, l'Acsé, l'Idex et le Pôle Grenoble Cognition de l'Univ. Grenoble Alpes, et le CRSH (Québec).

**MERCI !** à tous les co-auteurs de ces projets, dont : Ignacio Atal, Nathalie Bigras, Caroline Bouchard, Pascal Bressoux, Julie Chabert, Olivier Cosnefroy, Gwenaëlle Joët, Lise Lemay, Romain Laurent, Pleen Le Jeune, Christine Lequette, Laurent Lima, Vanda Luengo, Cécile Nurra, Anastasia Petrova, Dominique Vaufreydaz, & Philippe Wanlin

---
# Références (1/5)

<!-- _class: t-60 -->

- Abbott, A. (2016). L'avenir des sciences sociales. Entre l'empirique et le normatif. *Annales. Histoire, Sciences Sociales, 71*(3), 577—596. 
- Allen, J. P., Pianta, R. C., Gregory, A., Mikami, A. Y., & Lun, J. (2011). An Interaction-Based Approach to Enhancing Secondary School Instruction and Student Achievement. *Science, 333*(6045), 1034-1037. <https://doi.org/10.1126/science.1207998>
- Babad, E. (2007). Teachers' nonverbal behavior and its effects on students. In J. C. Smart (Ed.), *Higher education: Handbook of theory and research* (Vol. XXII, pp. 201—261). New York: Springer.
- Barrett, L. F. (2017). *How emotions are made. The secret life of the brain*. Boston: Houghton Mifflin Harcourt.
- Begrich, L., Kuger, S., Klieme, E., & Kunter, M. (2021). At a first glance — How reliable and valid is the thin slices technique to assess instructional quality? *Learning and Instruction, 74*. doi: 10.1016/j.learninstruc.2021.101466
- Biesta, G. (2020). *Educational research. An unorthodox introduction*. London: Bloomsbury.
- Bigras, N., Dessus, P., Lemay, L., Bouchard, C., & Lequette, C. (2020). Qualité de l'accueil d'enfants de 3 ans en centres de la petite enfance au Québec et en maternelles en France. *Enfances Familles Générations, 35*. 
- Bressoux, P. (2017). Practice-based research : une aporie et des espoirs. *Éducation et Didactique, 11*(3), 123-134. doi: 10.4000/educationdidactique.2870

---
# Références (2/5)

<!-- _class: t-60 -->

- Bressoux, P., Bianco, M., Bosse, M.-L., Cosnefroy, O., Dessus, P., Fayol, M., . . . Rocher, T. (2016). Rapport de la recherche LONGIT. Grenoble: Univ. Grenoble Alpes, LSE, Convention de recherche DEPP 2014-DEPP-026.
- Bryk, A. S. (2017). Accélerer la manière dont nous apprenons à améliorer. *Éducation et Didactique, 11*(2), 11—30. doi: 10.4000/educationdidactique.2796
- Connor, C. M., Morrison, F. J., Fishman, B., Giuliani, S., Luck, M., Underwood, P. S., . . . Schatschneider, C. (2011). Testing the Impact of Child Characteristics × Instruction Interactions on Third Graders' Reading Comprehension by Differentiating Literacy Instruction. *Reading Research Quarterly, 46*(3), 189—221. doi: 10.1598/RRQ.46.3.1
- Cortina, K. S., Miller, K. F., McKenzie, R., & Epstein, A. (2015). Where low and high inference data converge: Validation of CLASS assessment of mathematics instruction using mobile eye tracking with expert and novice teachers. *International Journal of Science and Mathematics Education, 13*(2), 389—403. doi: 10.1007/s10763-014-9610-5
- Cosnefroy, O., Nurra, C., Joët, G., & Dessus, P. (2015). *Analyse dynamique de la motivation des enfants en fonction de la nature des interactions élèves-enseignant*. 1re Journée de restitution de l'APR "Egalité des chances à l'école". Paris : MEN-DEPP.
- Dessus, P. (2003). Des outils cognitifs qui forment notre compréhension : une présentation de la théorie d'Egan. *Penser l'Education*(13), 71—87. 
- Dessus, P. (2007). Systèmes d'observation de classes et prise en compte de la complexité des événements scolaires. *Carrefours de l'Education, 23*, 103—117. 
- Dessus, P., Chabert, J., Pernin, J.-P., & Wanlin, P. (2020). Class-Card: a role-playing simulation of instructional experiences for pre-service teachers. In I. Marfisi-Schottman, F. Bellotti, L. Hamon, & R. Klemke (Eds.), 9th Int. Conf. Games and learning alliance (GALA 2020) (Vol. LNCS 12517, pp. 283–293). Springer. 


---
# Références (3/5)

<!-- _class: t-50 -->

- Dessus, P., Cosnefroy, O., & Luengo, V. (2016). "Keep your eyes on 'em all!": A mobile eye-tracking analysis of teachers' sensitivity to students. In K. Verbert, M. Sharples & T. Klobučar (Eds.), *Adaptive and adaptable learning. Proc. 11th European Conf. on Technology Enhanced Learning (EC-TEL 2016)* (pp. 72—84). New York: Springer.
- Dessus, P., Tanguy, F., & Tricot, A. (2015). Natural cognitive foundations of teacher knowledge: An evolutionary and cognitive load account. In M. Grangeat (Ed.), *Understanding science teacher professional knowledge growth* (pp. 187—202). Rotterdam: Sense Publishers.
- Donker, M. H., van Vemde, L., Hessen, D. J., van Gog, T., & Mainhard, T. (2021). Observational, student, and teacher perspectives on interpersonal teacher behavior: Shared and unique associations with teacher and student emotions. *Learning and Instruction, 73*. doi: 10.1016/j.learninstruc.2020.101414
- Doussot, S. (2018). Quelques conditions à l'institution d'une communauté d'amélioration. *Éducation et didactique*(12-3), 145-150. doi: 10.4000/educationdidactique.3650
- Downer, J. T., Booren, L. M., Hamre, B. K., Pianta, R. C., & Wiliford, A. P. (2010). inCLASS observation. Pre-K coding manual. Charlottesville: Univ. of Virginia, CASTL.
- Doyle, W. (2006). Ecological approaches to classroom management. In C. M. Evertson & C. S. Weinstein (Eds.), *Handbook of classroom management. Research, practice, and contemporary issues* (pp. 97—125). Mahwah: Erlbaum.
- Evertsen, C., Størksen, I., Tharaldsen, K. B., & Kucirkova, N. (2023). Gains and challenges with the Classroom Assessment Scoring System in a social pedagogical tradition. *Frontiers in Education, 7*. https://doi.org/10.3389/feduc.2022.965174 
- Egan, K. (1997). *The educated mind: How cognitive tools shape our understanding* : University of Chicago Press.
- Fauth, B., Decristan, J., Decker, A.-T., Büttner, G., Hardy, I., Klieme, E., & Kunter, M. (2019). The effects of teacher competence on student outcomes in elementary science education: The mediating role of teaching quality. *Teaching and Teacher Education, 86*. <https://doi.org/10.1016/j.tate.2019.102882> 
- Fauth, B., Wagner, W., Bertram, C., Göllner, R., Roloff, J., Lüdtke, O., . . . Trautwein, U. (2020). Don't blame the teacher? The need to account for classroom characteristics in evaluations of teaching quality. *Journal of Educational Psychology, 112*(6), 1284-1302. doi: 10.1037/edu0000416
- Flanders, N. A. (1976). Analyse de l'interaction et formation. In A. Morrison & D. McIntyre (Eds.), *Psychologie sociale de l'enseignement*(Vol. 1, pp. 57—69). Paris: Dunod.
- Hoc, J.-M. (1996). *Supervision et contrôle de processus*. Grenoble: P.U.G.

---
# Références (4/5)

<!-- _class: t-50 -->

- Hochschild, A. R. (2017). *Le prix des sentiments. Au cœur du travail émotionnel*. Paris: La Découverte.
- Hora, M. T., Oleson, A., & Ferrare, J. J. (2013). Teaching Dimensions Observation Protocol (TDOP) user's manual. Madison: Wisconsin Center for Education Research, University of Wisconsin-Madison.
- Jones, D. E., Greenberg, M., & Crowley, M. (2015). Early socio-emotional functioning and public health: The relationship between kindergarten social competence and future wellness. *American Journal of Public Health*, e1-e8. doi: 10.2105/ajph.2015.302630
- Kelly, S., Bringe, R., Aucejo, E., & Cooley Fruehwirth, J. (2020). Using global observation protocols to inform research on teaching effectiveness and school improvement: Strengths and emerging limitations. *education policy analysis archives, 28*. doi: 10.14507/epaa.28.5012
- Koschmann, T. (2017). The Quest for Measurement. *Éducation et Didactique, 11*(3), 143-146. doi: 10.4000/educationdidactique.2873
- Kounin, J. S. (1970). *Discipline and group management in classrooms*. New York: Holt, Rinehart & Winston.
- Landauer, T. K., & Dumais, S. T. (1997). A solution to Plato's problem: the Latent Semantic Analysis theory of acquisition, induction and representation of knowledge. *Psychological Review, 104*(2), 211—240. 
- Laurent, R., Vaufreydaz, D., & Dessus, P. (2020). Ethical teaching analytics in a Context-Aware Classroom: A manifesto. *ERCIM News, 120*, 39—40. 
- Lemaire, B., & Dessus, P. (2003). Modèles cognitifs issus de l'Analyse de la sémantique latente. *Cahiers Romans de Sciences Cognitives, 1*(1), 55—74. 
- Lima, L., Bressoux, P., & Dessus, P. (2020). *Réduction de la taille des classes : évaluer les effets au-delà des modifications de performances*. Paper presented at the 32e Colloque de l'ADMEE Europe., Casablanca. 
- Marcel, J.-F. (2017). Éditorial. Observation de la qualité et qualité de l’observation. *Les Dossiers des Sciences de l'Education, 37*, 7–14. 
- Martin, O. (2020). *L'empire des chiffres*. Paris : Colin.
- McIntyre, N. A., Jarodzka, H., & Klassen, R. M. (2017). Capturing teacher priorities: Using real-world eye-tracking to investigate expert teacher priorities across two cultures. *Learning and Instruction*. doi: 10.1016/j.learninstruc.2017.12.003
- OCDE. (2019). Résultats de TALIS 2018 (vol. I). Paris : rapport OCDE.

---
# Références (5/5)

<!-- _class: t-50 -->

- Molina, E., Pushparatnam, A., Rimm-Kaufman, S., & Wong, K. K.-Y. (2018). Evidence-Based Teaching. Effective Teaching Practices in Primary School Classroom. Washington: World bank group.
- Nuthall, G. (2007). *The hidden lives of learners*. Wellington: NZER Press.
- Pelletier, M.-A. (2022). La conscience de soi des enseignantes et des enseignants à l’éducation préscolaire : un domaine de compétences socio-émotionnelles à explorer dès la période d’insertion professionnelle. *Éducation & Francophonie, 50*(2). https://doi.org/10.7202/1097034ar 
- Petrova, A., Vaufreydaz, D., & Dessus, P. (2020). *Group-level emotion recognition using a unimodal privacy-safe non-individual approach.* Paper presented at the 8th Emotion Recognition in the Wild Challenge (EmotiW) Int. Conf., Joint to the ACM Int. Conf. on Multimodal Interaction (ICMI 2020), Utrecht.
- Pianta, R. C. (1999). *Enhancing relationships between children and teachers*. American Psychological Association. 
- Pianta, R. C., La Paro, K. M., & Hamre, B. K. (2008). *Classroom assessment scoring system[CLASS] Manual: Pre-K*. Baltimore: Brookes.
- Pianta, R. C., Lipscomb, D., & Ruzek, E. (2022). Indirect effects of coaching on pre-K students' engagement and literacy skill as a function of improved teacher—student interaction. *Journal of School Psychology, 91*, 65-80. <https://doi.org/10.1016/j.jsp.2021.12.003>  
- Praetorius, A.-K., Klieme, E., Herbert, B., & Pinger, P. (2018). Generic dimensions of teaching quality: the German framework of Three Basic Dimensions. *Zdm, 50*(3), 407-426. doi: 10.1007/s11858-018-0918-4
- Quesada, J., & Gomez, E. (2002). *Latent Problem Solving Analysis as an explanation of expertise effects in a complex, dynamic task.*Paper presented at the Annual Meeting of the Cognitive Science Society, Boston.
- Rogalski, J. (2003). Y a-t-il un pilote dans la classe ? Une analyse de l'activité de l'enseignant comme gestion d'un environnement dynamique ouvert. *Recherche en Didactique des Mathématiques, 23*(3), 343—388. 
- Schneider, M., & Preckel, F. (2017). Variables associated with achievement in higher education: A systematic review of meta-analyses. *Psychological Bulletin, 143*(6), 565—600. doi: 10.1037/bul0000098
- Uttl, B., White, C. A., & Gonzalez, D. W. (2017). Meta-analysis of faculty's teaching effectiveness: Student evaluation of teaching ratings and student learning are not related. *Studies in Educational Evaluation, 54*, 22-42. doi: 10.1016/j.stueduc.2016.08.007
- Vinsel, L., & Russell, A. L. (2020). *The innovation delusion*. New York: Currency.
- Wolff, C. E. (2015). *Revisiting 'withitness': Differences in Teachers' Representations, Perceptions, and Interpretations of Classroom Management* : Heerlen: Open Universiteit of the Netherlands.
- Zhao, Y. (2017). What works may hurt: Side effects in education. *Journal of Educational Change, 18*(1), 1-19. https://doi.org/10.1007/s10833-016-9294-4 

---
# ANNEXE : Description des autres domaines du CLASS

Version Pre-K ou K-3 (Petite section au CE2)

---
# 2. Domaine 2 – Organisation de la classe
Comment l'enseignant organise et gère différents aspects de la classe (comportement des élèves, temps, attention). Il y a une meilleure opportunité d'apprentissage et la classe fonctionne mieux si les élèves ont un comportement adéquat, cohérent avec les tâches qui leur sont allouées, et sont intéressés et engagés dans ces dernières.

---
# 2. Organisation classe - 5. Gestion des comportements (GC)
Englobe l'habileté de l'enseignant à établir des attentes comportementales claires et à utiliser des méthodes efficaces pour prévenir et rediriger les comportements inappropriés

**Indicateurs**
- Attentes comportementales claires
- Proactivité
- Redirection des comportements
- Comportements des enfants

---
# 2. - Organisation classe  – 6. Productivité (P)

Dans quelle mesure l'enseignant gère le déroulement des activités et des routines de manière à optimiser le temps disponible à l'apprentissage ?

**Indicateurs**
- Maximisation du temps d'apprentissage
- Routines
- Transitions
- Préparation du matériel nécessaire à la séance

---
# 2. - Organisation classe - 7. Modalités d'apprentissage (MA)

Reflète la manière dont l'enseignant maximise l'intérêt des enfnats, leur engagement et leur capacité à apprendre au cours des leçons et activités offertes

**Indicateurs**
- Accompagnement efficace
- Diversité des modalités et des matériels
- Intérêt de l'enfant
- Clarté des objets d'apprentissage

---
# 2. - Domaine 3 – Soutien à l'apprentissage

S'intéresse à décrire comment les enseignants aident les enfants à 
- apprendre à résoudre des problèmes, à raisonner et à penser
- utiliser les rétroactions pour approfondir des habiletés et des connaissances
- développer des habiletés langagières plus élaborées

---
# 2.  Soutien appr — 8. Développement de concepts (DC)

Utilisation par l'enseignant de stratégies diverses (discussions, activités) afin d'amener l'enfant à développer une pensée plus élaborée, une plus grande compréhension des phénomènes de son environnement plutôt que de favoriser un apprentissage par cœur

**Indicateurs**

- Analyse et raisonnement
- Créativité
- Intégration d'éléments déjà rencontrés dans les cours précédents
- Lien avec la vie réelle

---
# 2. - Soutien appr — 9. Qualité de la rétroaction (QR)

Le niveau selon lequel l'enseignant offre des rétroactions aux enfants qui optimisent l'apprentissage et qui favorisent une participation maintenue

**Indicateurs**
- Étayage
- Rétroactions en boucle
- Stimuler les processus de la pensée
- Fournir de l'information
- Encouragement et affirmation
  
---
# 2. - Soutien appr — 10. Modelage langagier (ML)

Rend compte de l'efficacité et de la quantité des techniques de stimulation du langage utilisées par l'enseignant.

**Indicateurs**
- Conversations fréquentes
- Questions ouvertes
- Répétition et extension
- *Self-talk* et *parallel-talk*
- Niveau de langage élaboré
