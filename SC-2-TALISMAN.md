---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)

## TALISMAN Project
## Steering Committee #2
## Contribution LaRAC-UGA
## 9 nov. 2023

---
<!-- paginate: true -->
<!-- footer: Steering Committee #2 - ANR-TALISMAN - 9/11/23 CC:BY-NC-SA -->

---
# Vue générale des doc. et post-doc impliqués (à compléter)


![100%](images/view-doc.jpeg)

---
# Point LaRAC-UGA


---
# Personnes nouvellement impliquées dans TALISMAN

- **1 nov. 2023** : Recrutement en contrat doctoral d'Aurélie Huret (travail sur les déterminants de l'engagement analysables en CAC)
-  **Novembre 2023-Mai 2024** : Accueil d'Aurore Rèmes, stagiaire M2 MEEF-PIF, Univ. Poitiers 
	-  travail sur Intenses, l'outil d'observation humaine de l'engagement dans le supérieur, conçu par L. Lardy & Ph. Dessus
	-  travail sur un outil de formation professionnelle des enseignants les amenant à réfléchir à leur enseignement et l'engagement des étudians
-  Premiers pourparlers d'accueil et/ou de co-encadrement d'un doctorant de sciences de gestion : Étude qualitative de la prise en compte de l'éthique au sein de l'équipe de chercheurs.

---
# Point avancement WPA (Deliverable A.1)

- 1 :white_check_mark: Introduction
- 2 :white_check_mark: Legal issues (version 4, en VF, donc v. anglaise à prévoir)
- 3 :white_check_mark: Ethical issues  (relu, en attente de commentaires)
- 4 :white_check_mark: Partie décrivant succinctement le doc. CERGA (à joindre en annexe, **soumis au CERGA-UGA le 8/11/23**)
- 5 :white_check_mark: Questionnaire éthique (R. Laurent)

---
# WPA (DA.1) À Faire courant novembre 2023

- Ecrire l'*Executive summary* et la conclusion (PhD)
- Décider où insérer (si on l'insère) le lexique juridique de Kanto (Annexe?)
- Insérer le doc. CERGA en annexe et le soumettre au CERGA
- Finaliser le questionnaire  éthique de Romain, le présenter en Section 5 du DA.1
- Tout relire

**Livraison finale prévue du DA.1 : fin novembre au plus tard**

---
# Questions en suspens

- Prévoir un addendum au dossier CERGA décrivant les expés de Poitiers
- Discuter de la manière dont les données seront partagées entre Grenoble et Poitiers 
- Qui seront les responsables de traitement ? Les présidents ? Les responsables du projet sur les 2 sites (Dominique et Christine)
- Commencer à réfléchir au partage des données inter-sites : quelles données de Poitiers seront traitées à Grenoble, et vice versa

---
# La suite : DA.2

- Les études prévues pour le DA.2 à réaliser à la suite du projet ARIANE (porteure S. Cojean)
  - Étude où des participants suivent un cours dans la CAC, avec questionnaires sur l’acceptabilité en pré/post. 
  - En questionnaire post on pourrait manipuler 1 autre facteur, sur la connaissance des traitements réalisés : 
    - 1 groupe à qui on explique de près les traitements (posture, dir. tête, etc.) en les leur montrant, 
    - l’autre qu’on laisse plus dans le vague
  - possibilité de travailler sur les différents facteurs jouant un rôle sur l'acceptabilité (*a priori*) et acceptation (après un premier usage)
  - comparer l'acceptabilité des capteurs proximaux vs. distaux.

---
# D.A3

- Recherche de collaborations avec collègues qui travaillent dans des directions proches (conception et test d'un “jeu sérieux“ pour amener des problématiques éthiques). 
- Deux pistes à creuser 
	- [Martin Ragot](https://www.researchgate.net/profile/Martin-Ragot), entreprise IRT b-com (contact : Salomé) : hip hip hip IA
	- [Ann-Louise Davidson](https://www.concordia.ca/faculty/ann-louise-davidson.html), Concordia univ., Canada, Jeu “Ethical Pursuit”