---
marp: true
paginate: true
autoscale: true
theme: uga
---
<!-- _paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)

## Transformation d'UE à l'Inspé
### Organisation et mise à jour de la base de documents de cours sur le numérique et les sciences de l'éducation
Christophe Charroud, Philippe Dessus, & Laurence Osete
Inspé, Univ. Grenoble Alpes


---
# URL de cette présentation

- https://link.infini.fr/transfo-ue-uga

---
<!-- footer: Transformation UE-Base de cours Inspé | Charroud, Dessus, & Osete | 14/11/24 | CC:BY-NC-SA-->
<!-- paginate: true -->

# La vie des enseignants : faire  des ressources 

* Enseigner implique de produire..., 
	* ... individuellement et collectivement..., 
	* ... un grand nombre de types **différents** de documents (cours, TP, TD, présentations, etc.),
	* ... destinés à des niveaux d'étudiants **différents**,
	* ... avec une structure **sophistiquée**, et des échéances de révision rapprochées.

---
# Économiser pour éviter de toujours réinventer la roue

* Beaucoup de temps est alloué à cette production, **pas toujours optimalement** : documents propriétaires avec de multiples versions, difficiles à modifier et convertir, forme souvent non harmonisée, etc.
* :dart: Utilité d'un système pouvant aider à produire, stocker, et diffuser ces documents 

---
# Des ressources libres et ouvertes ?

- de licence de partage **libre**
- de format de fichier **ouvert**
- **accessibles**/utilisables par tous (*responsive*) et de partout (sur site web non protégé par authentification)

---
# Une précision : Les licences *Creative Commons*

- Ce [site](https://chooser-beta.creativecommons.org) permet de choisir sa licence selon ce qu'on veut que les utilisateurs fassent des ressources *sans requête aux auteurs*
  - Attribution : mention des auteurs : BY, ou pas : 0
  - Usage non-commercial : NC
  - Création de dérivés impossible : ND
  - Usage de la même licence si dérivés : SA

- Nos ressources sont généralement sous : BY-NC-SA

---
# Sphinx et Marp

- Pour la production des documents de cours, nous avons opté pour [Sphinx](http://sphinx-doc.org), un outil de **génération de documentation** utilisant [Markdown](https://fr.wikipedia.org/wiki/Markdown)
- Et pour les présentations, pour [Marp](https://marpit.marp.app). Génère un PDF et un HTML directement présentables en cours
- Ces 2 systèmes sont hébergés gratuitement sur le serveur Gricad de l'UGA

---
# Caractéristiques principales des documents
	
* **structurés sémantiquement**, pouvant s'adapter à des styles différents
* **modulaires & accessibles** 
* avec tous les **attributs** des cours (index, renvois croisés, images, vidéos, schémas, formules math, etc.)
* gérant des styles bibliographiques (BibTeX)
* comportant des **QCM**
* pouvant admettre de nombreuses extensions (e.g., [GraphViz](https://graphviz.org))
* **éditables** collaborativement en asynchrone
* intégrables dans **toute plate-forme** d'enseignement  (e.g., *Moodle*)

---
# Portail de ressources à l'Inspé, UGA

- Portail d'environ **200 documents de cours** (principalement sur le numérique et les sciences de l'éducation)
- [Index des cours](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/gene/)
- [Index des présentations](https://gricad-gitlab.univ-grenoble-alpes.fr/inspe-sciedu/slides/-/blob/main/README.md)

- **Cours** plutôt théoriques avec QCM intégrés
- **Ateliers** (TD courts)
- **Tutoriels** (TP expliquant une procédure)
- **Ressources** (listes raisonnées et thématiques d'URL)
- **Syllabi** (programmes de cours)
- **Parcours** (pages pointant sur les ressources précédentes, par thème)

--- 
# Le travail de transformation d'UE

1. Faire le ménage dans les documents
2. Mieux relier thématiquement les différents documents par des renvois et des “parcours“ thématiques
3. Ajouter des QCM à certains documents les plus utilisés
4. Proposer des tutoriels sur la recherche en éducation

---
# Démo

- Les [cours de méthode de la recherche](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/rech-educ/index.html)
- Une [présentation](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/UE14.html) en HTML
- Une [présentation](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/UE14.pdf) en PDF
- Un fichier-source

---
# Conclusion

- Site de ressources très utilisé, tous nos cours sont travaillés collaborativement
- Rayonnement possible puisqu'accessible et sous licence libre
- Recul suffisant (une dizaine d'années) sur les outils techniques
- :loudspeaker: :loudspeaker: **N'hésitez pas à nous contacter si vous voulez contribuer au site ou créer le vôtre** 

---
# Merci pour votre attention !

## Références

Pour plus d'informations : 
* Dessus, P., & Besse, É. (2020). [Des ressources de cours libres et collaboratives pour une formation hybride des enseignants : Design et impact](https://journals.openedition.org/dms/5252*). *Distances et Médiations des Savoirs*, *31*. 
* Ce projet a été partiellement financé par l'[IDEFI numérique ReFlexPro](https://www.idefi-reflexpro.fr)
* Merci à [Naeka](https://www.naeka.fr) pour avoir créé le système initial, et à Franck Pérignon, LJK, UGA, pour avoir permis le transfert dans le système actuel, et à Fabrice Ménard, DAPI, UGA, pour son aide attentive tout au long de ce projet