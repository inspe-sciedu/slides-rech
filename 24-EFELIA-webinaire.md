---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
  
# Pour une littératie de l'IA générative chez les enseignants et étudiants
 
## Philippe Dessus - LaRAC, LIG, Inspé, UGA
## Daniel Seyve - DAPI, UGA

---
<!-- page_number: true -->
<!-- paginate: true -->
<!-- footer: Littératie GenIA – Ph. Dessus & D. Seyve - Webinaire MIAI-Minalogic-UGA - 29/11/2024 - CC:BY-NC-SA -->
# Abréviations et définitions

- IA générative : **GenIA**
  
- URL de cette présentation : https://link.infini.fr/miai-genia
- Les travaux cités sont référencés en fin de présentation

---
# Constat et décision

- **Constats** : Fortes attentes sur l'IA !
  - **Fortes attentes des enseignant·es** en matière de formation et informations sur la GenIA
  - Démarche volontariste de P. Hetzel, ministre de l'ESR, d'“**Acculturer, former et faire adopter l'IA partout**”, notamment en l'intégrant comme outil pédagogique (Priorité 2, MESR, 2024)
- **Décision locale** : Développer une littératie de l’IA chez les enseignant·es (et les étudiant·es) de l'UGA 

---
# Littéracie de l'IA

La littératie de l'IA est définie comme (Chiu et al., 2024, p. 4) 
> la capacité d'un individu à expliquer clairement comment **les technologies de l'IA fonctionnent et ont un impact sur la société**, ainsi qu'à les utiliser de manière **éthique et responsable**, et à **communiquer efficacement** avec elles dans n'importe quel contexte. Elle se concentre sur le savoir (*i.e.*, les connaissances et les compétences) 

(nous soulignons)

---
# Programme d'une session de sensibilisation à la GenIA

- Les grands **principes de fonctionnement** de la GenIA, focus sur les robots conversationnels
- Examiner les **enjeux sociaux**, notamment économiques, culturels et environnementaux
- État des lieux et perspectives de **l’usage de l’IA** dans les universités (sélection, tutorat, évaluation, individualisation)
- Échanges sur les **postures à adopter** face à ces technologies : lutter contre, faire avec, ou les intégrer pleinement

---
# Participation

- À ce jour, **90 enseignant·es** y ont participé en **8 sessions**
- D'autres sessions prévues d'ici à fin 2024

---
# Bilan des sessions

- Les enseignant·es **doivent être associé·es** aux réflexions politiques et pédagogiques sur la GenIA
- Répond aux besoins de **compréhension des principes de fonctionnement** de la GenIA
- **Difficile de lutter** contre la GenIA… “faire avec” vs. “adopter”
- Attentes fortes sur la **mise en place d’ateliers** permettant 
  - de tester des GenIA 
  - de créer des activités pédagogiques les utilisant
  - de créer des *chatbots* privés
- Demande d'adoption d'une **charte d'usage** au niveau de l'université

---
# Questions à approfondir

- Comment analyser empiriquement les **importants biais** (notamment de genre) de la GenIA ? (Fulgu & Capraro, 2024)
- L'humain va-t-il **rester dans la boucle** ? La GenIA génère des sujets :arrow_right: y répond :arrow_right: les corrige 
- Comment analyser les aspects **RGPD, éthiques** de la GenIA ?
- Quels **modèles économiques** d'utilisation de la GenIA au niveau UGA (interne vs. abonnement externe) ?
- Quid des **spécificités des disciplines** ? (en pharmacie vs. en langues/traductologie)

---
# Une piste de travail : “démagifier” la GenIA

- La GenIA est massivement **anthropomorphisée et “magifiée”**, par ses concepteurs, ses vendeurs, mais aussi certains chercheurs
- **Stratégie commerciale** (en partie) qu'il faut détricoter car elle augmente la confiance envers les systèmes et obscurcit les raisonnements et décisions sur la GenIA (masque les problèmes)
- Les processus de GenIA sont **éloignés des processus psychologiques humains** — on est très loin d'une IA générale (Dentella et al., 2024 ; Floridi & Nobre, 2024 ; Mitchell, 2024)
- Des ateliers de **design d'outils d'IA** (Lupetti & Murray-Rust, 2024) peuvent permettre de prendre du recul sur cette “magification” (en en expliquant le fonctionnement, élicitant les opinions, les faisant manipuler)

---
# Merci de votre attention ! 
# Des :question:

<!-- _class: t-60 -->

## Références

- Chiu, T. K. F., Ahmad, Z., Ismailov, M., & Sanusi, I. T. (2024). [What are artificial intelligence literacy and competency? A comprehensive framework to support them](https://doi.org/10.1016/j.caeo.2024.100171). *Computers and Education Open, 6*.
- Dentella, V., Gunther, F., Murphy, E., Marcus, G., & Leivada, E. (2024). [Testing AI on language comprehension tasks reveals insensitivity to underlying meaning](https://doi.org/10.1038/s41598-024-79531-8*). *Scientific Reports, 14*(1), 28083. 
- Dessus, P. (2024). [Parcours sur la GenIA à l'université](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/qcm/parcours-ia-gen.html). Grenoble : UGA.
- Floridi, L., & Nobre, A. C. (2024). [Anthropomorphising Machines and Computerising Minds](https://doi.org/10.1007/s11023-024-09670-4). *Minds and Machines, 34*(1).
- Fulgu, R. A., & Capraro, V. (2024). Surprising gender biases in GPT. [Computers in Human Behavior Reports](https://doi.org/10.1016/j.chbr.2024.100533), 16.    
- Lupetti, M. L., & Murray-Rust, D. (2024). [(Un)making AI Magic: A Design Taxonomy](https://doi.org/10.1145/3613904.3641954). *Proc. of the CHI Conference on Human Factors in Computing Systems.*
- MESR (2024, 19 nov.) [Patrick Hetzel présente sa feuille de route](https://www.enseignementsup-recherche.gouv.fr/fr/feuille-de-route-2024)
- Mitchell, M. (2024). [The metaphors of artificial intelligence](https://doi.org/10.1126/science.adt6140*). *Science, 386*(6723), eadt6140.