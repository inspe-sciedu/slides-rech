---
marp: true
paginate: true
autoscale: true
theme: uga
---
<!-- _paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)

## Libérez les ressources d'enseignement !
### Des manuels et présentations libres et ouvertes avec Gitlab à l'université
Philippe Dessus (1) & Fabrice Ménard (2)
 (1) LaRAC, Inspé, Univ. Grenoble Alpes
 (2) DAPI, Univ. Grenoble Alpes


<!-- GT “Données”, MITI CNRS | 29/06/23 | CC:BY-NC-SA-->


---
# La vie des enseignants : faire  des ressources 

* Enseigner implique de produire..., 
	* ... individuellement et collectivement..., 
	* ... un grand nombre de types **différents** de documents (cours, TP, TD, présentations, etc.),
	* ... destinés à des niveaux d'étudiants **différents**,
	* ... avec une structure **sophistiquée**, et des échéances de révisions rapprochées.

---
# Économiser pour éviter de toujours réinventer la roue

* Beaucoup de temps est alloué à cette production, **pas toujours optimalement** : documents propriétaires avec de multiples versions, difficiles à modifier et convertir, forme souvent non harmonisée, etc.
* Utilité d'un système pouvant aider à produire, stocker, et diffuser ces documents 

---
# Des ressources libres et ouvertes ?

- de licence de partage **libre**
- de format de fichier **ouvert**
- **accessibles**/utilisables par tous (*responsive*)
- permettant une **évaluation** de ce qui est compris


---
# *Massive Open Online Textbook*

:book: "un outil d’enseignement on-line et flexible qui combine du texte "cherchable", commentable, des outils multimédia pour une collaboration pair-à-pair, et des rétroactions" [Hall, 2013](http://www.newappsblog.com/2013/09/we-need-moots-not-moocs.html>)

* Passer du temps sur **le contenu et sur l'innovation**, moins à évaluer, donner des rétroactions, ou détecter le plagiat (Hall, 2013)

* Quel outil pour les créer et diffuser ? Un simple site internet peut suffire, mais rend difficile sa mise à jour collaborative



---
# Les langages de balisage léger

- L'objectif d'un langage de balisage léger est de faciliter la création de documents lisibles par les humains et également facilement analysables par les machines. Il est utilisé pour dans le cas où un langage de balisage complet comme le XML ou le HTML serait trop complexe.


- Nous avions commencé par utiliser reStructuredText (rst) qui posède beaucoup de fonctionalité, puis nous avons migrer vers le MarkDown (étendu de quelques extensions) qui plus répandu et plus simple à apréhender pour les collègues. 

---
# Les interpreteurs

L'interpréteur génère le rendu final du document. Cela peut être sous la forme d'un fichier HTML, d'un document PDF, d'un affichage à l'écran ou d'un autre format de sortie pris en charge.



Pour la production de présentationss nous avons choisi [Marp](http://marp.app) qui utilise le [Markdown](https://daringfireball.net/projects/markdown/).

Pour la production des documents de cours, nous avons opté pour [Sphinx](http://sphinx-doc.org) qui est un outil de **génération de documentation**, à l'origine conçu pour établir la documentation du langage *Python*

---
# Pandoc

![bg left h:500](https://pandoc.org/diagram.svgz?v=20230606212820)
La migration a été faite au moyen de [Pandoc](https://pandoc.org/)  qui est le véritable couteau suisse des langages de balisage léger.
Il permet par exemple de convertir un doxument docx en Markdown.
Nous réfléchissons à éventuellement l'utiliser directement à la place de Sphinx Doc et / ou Marp-it

---
# Le fonctionnement

Nous utilisons les fonctionalités suivantes de [GitLab](https://about.gitlab.com)

- Intégration continue et deploiement
- les Pages
- les containers Docker

---
# Caractéristiques principales des docs
	
* **structurés sémantiquement**, pouvant s'adapter à des styles différents
* **modulaires & accessibles** 
* avec tous les **attributs** des cours (index, renvois croisés, images, vidéos, schémas, formules math,  etc.)
* gérant des styles bibliographiques
* comportant des **QCM**
* pouvant admettre de nombreuses extensions (e.g., Graphviz)
* **éditables** collaborativement en asynchrone
* intégrables dans **toute plate-forme** d'enseignement  (e.g., *Moodle*)

---
# Portail de ressources à l'Inspé, UGA

Portail d'environ **200 documents de cours** (principalement sur le numérique et les sciences de l'éducation)
[Index des cours](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/gene/)
[Index des présentations](https://gricad-gitlab.univ-grenoble-alpes.fr/inspe-sciedu/slides/-/blob/main/README.md)

- **Cours** plutôt théoriques avec QCM intégrés
- **Ateliers** (TD courts)
- **Tutoriels** (TP expliquant une procédure)
- **Ressources** (listes raisonnées et thématiques d'URL)
- **Syllabi** (programmes de cours)
- **Parcours** (pages pointant sur les ressources précédentes, par thème)

---
# Démo

---
# Merci pour votre attention !

## Références

Pour plus d'informations : 
* Dessus, P., & Besse, É. (2020). [Des ressources de cours libres et collaboratives pour une formation hybride des enseignants : Design et impact](https://journals.openedition.org/dms/5252*). *Distances et Médiations des Savoirs*, *31*. 
* Merci à [Naeka](https://www.naeka.fr) pour avoir créé le système initial, et à Franck Pérignon, LJK, UGA, pour avoir permis le transfert dans le système actuel.