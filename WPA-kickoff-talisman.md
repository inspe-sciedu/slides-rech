---
marp: true
paginate: true
autoscale: true
theme: uga
---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)

## Séance de lancement de TALISMAN
### Projet WPA - et partie de WP1
### LaRAC, Univ. Grenoble Alpes
### *À partir de la présentation de Romain Laurent*

---
<!-- WPA - ANR-TALISMAN - CC:BY-NC-SA-->
<!-- paginate: true -->

# Objectifs du WPA

- **A.1** - Assurer un traitement légal des données (conformité RGPD) 
- **A.1** - Construire un cadre éthique aux expérimentations 
- **A.2** - Proposer un questionnaire d'utilisabilité de la CAC, fondé expérimentalement
- **A.3** - Conception et test d'un jeu sérieux facilitant la passation du questionnaire précédent

---
# A.1 Revue éthique et légale de la CAC

- Instruction RGPD
- Instruction CERGA (comité éthique pour les recherches non interventionnelles)
- Construction d'un cadre de questionnement éthique (lien avec le projet MaDICS)

---
# A.2 Questionnaire d'acceptabilité de la CAC

- Validation d'un questionnaire d'acceptabilité de la CAC, préparant l'étude de leur engagement comportemental dans une CAC (*cf*. WP 1.2)
- Cadre théorique en cours de discussion : centration sur la technologie (utilisabilité) ? sur le comportement (théorie du comportement planifié) ? mélange des 2 ?
- Étude pilote
- Construction et validation psychométrique du questionnaire pilote
- Administration du questionnaire avant/après utilisation d'une CAC

---
# A.3 Création d'un jeu sérieux

- Jeu sérieux (en ligne ?) informant, facilitant et ludifiant la passation du questionnaire de prédiction du comportement 

---
# Rappels des livrables WPA

- **DA.1** (T0+9) - *Vademecum* de l’utilisation éthique et légale d’une CAC (Resp. PhD)
- **DA.2** (T0+15) - Questionnaire sur l'acceptabilité de la CAC (Resp. SC+RL)
- **DA.3** (T0+18) - Jeu sérieux (RL)

---
# Contribution (possible) du LaRAC au WP 1

- **WP1.1** - Revue des différentes mesures de l'engagement comportemental/émo. dans une perspective de les mesurer automatiquement (LL/PhD/SC)
- **WP 1.1** - Revue sur la pédagogie naturelle pour construire des actions péda. unitaires et analysables automatiquement (PhD/X)
- **WP 1.1** - Construction et test d'un système d'obs. des interactions ens.-étudiants dans le sup (LL/PhD)
- **WP 1.2** - Revue systématique des CACs d'un point de vue péda et technique (AA/DV/FL/PhD)

---
# À plus long terme...

- **WP1.2** - Observation multimodale de classes pour le développement professionnel des enseignants (lien avec WP3)
- Revue des différentes approches pour analyser les événements univ. in vivo (PhD/X, voir liens avec MANIP/IDEE?)
- Un modèle pour décrire le flux de capture et analyse des interactions dans les CAC (*Context-Aware Classrooms : An Ecological Psychology Framework*, PhD/RL/DV, papier ICALT, voir liens avec MANIP/IDEE?)
  

---
# Questions restantes

- Va-t-on vers une mesure de l'engagement de l'enseignant ? Avec quels modèles théoriques ?
- Approche multiple ou unifiée (et si oui, comment ?) des types d'interactions dans les CAC ?

---
# WPA. Point au 17/11/23
Visioconférence Grenoble-Poitiers
**Présents** : Aurore R., Aurélie H., Romain L., Salomé C., Daria V., Fiorella A., Francis J., Tanja P., Philippe D.

---
# Point avancement WPA (Deliverable A.1)

- 1 :white_check_mark: Introduction
- 2 :white_check_mark: Legal issues (version 4, en VF, donc v. anglaise à prévoir)
- 3 :white_check_mark: Ethical issues  (relu, en attente de commentaires)
- 4 :white_check_mark: Partie décrivant succinctement le doc. CERGA (à joindre en annexe, **soumis au CERGA-UGA le 8/11/23**)
- 5 :white_check_mark: Questionnaire éthique, *Value Sensitive Design* (R. Laurent)


---
# Points discutés pendant la réunion  (1/2)

<!-- _class: t-40 -->

- :white_check_mark:  Ordre du doc. : légal puis éthique OK
- :white_check_mark: Co-autorat : interclassement des auteur·es entre Grenoble & Poitiers
- :white_check_mark: Ajout Hassina comme co-autrice (**PhD**)
- Faire une introduction un peu plus large (notamment avec la question des normes/légal/éthique de TP/RL), penser à faire, dans chaque section, des intro entonnoirs (CAC en général, puis CAC TALISMAN)
- :white_check_mark:  Voir les textes en rouge de la partie Legal Issues (**RL/PhD**)
- Nécessité de faire un AIPD ? attendre avis CERGA (déjà débuté par PhD)
- Droit à l'explicabilité ? Pas de nécessité si le système ne prend pas de décision scolaire (note, orientation, etc.) (TP)

---
# Points discutés (2/2)

- En fin de partie éthique, reboucler sur le projet de règlement UE (**TP**)
- :white_check_mark: Sourcer Tab. II (Elliott)  (**PhD**)
- Augmenter Tab. II (Elliott) afin de voir si d'autres valeurs ne seraient pas utiles (benvolence dans steering res.) (**RL**) ; voir aussi si des valeurs du règlement IA UE ne pourraient pas être présentes dans ce tab (**TP**)
- Le lexique de Kanto ne sera pas inséré dans le Livrable, mais des bouts seront repris dans l'intro, pour situer le débat sur légal/éthique/déontologique... (**TP/RL**)

---
## A faire courant novembre 2023

- Ecrire l'*executive summary* et la conclusion (PhD)
- Finaliser legal/ethical Issues
- Insérer le doc. CERGA en annexe (**PhD**)
- Finaliser le questionnaire de Romain, le présenter en section 5 du DA.1 (**RL**)
- Tout relire (**PhD/TP**)
- Présenter les applications à TALISMAN dans un style différent (indenté, avec un bandeau gris) (**PhD**)


---
# Échéance

**Livraison finale prévue du DA.1 : fin novembre, tout début décembre 2023 au plus tard**

---
# La suite 1/2

- DV est très intéressée par la question de l'explicabilité, la transparence, et l'open Sci. Voir qui d'autre serait intéressé.e pour faire un sous-groupe de travail sur la question
- RL/TP intéressés à faire une revue systématique sur les CAC sous l'angle légal/éthique (quelles valeurs embarquées dans les CACs “permises“ par les processus de capture/analyse embarqués) ?


---
## La suite 2/2 (DA.2 et DA.3)
### D.A2
- On va voir à faire en sorte  que les études décrites dans le DA.2 soient réalisées à la suite du projet ARIANE (porteure SCojean), ce qui donnerait un autre contexte que celui initial (salles CAC plutôt que systèmes info). 
- On pourrait envisager une étude où des participants suivent un cours dans la CAC, avec questionnaires sur l’acceptabilité en pré-post. Pour le questionnaire post on pourrait manipuler 1 autre facteur, sur la connaissance des traitements réalisés : 1 groupe à qui on explique de près les traitements (posture, dir. tête, etc.) en les leur montrant, l’autre qu’on laisse plus dans le vague.
- possibilité de travailler sur les différents facteurs jouant un rôle sur l'acceptabilité (a priori) et acceptation (après un premier usage)
- comparer l'acceptabilité des capteurs proximaux vs. distaux.

---
# D.A3
- Recherche de collaborations avec collègues qui travaillent dans des directions proches. 3 pistes
	- [Martin Ragot](https://www.researchgate.net/profile/Martin-Ragot), entreprise IRT b-com (contact : Salomé) : hip hip hip IA
	- [Ann-Louise Davidson](https://www.concordia.ca/faculty/ann-louise-davidson.html), Concordia univ., Canada, Jeu “Ethical Pursuit”
	- Négociations en cours avec directrice d'un doctorant en sciences de gestion qui veut travailler sur l'éthique en EdTech ; intéressé par TALISMAN et la conception d'un jeu sérieux sur l'éthique






