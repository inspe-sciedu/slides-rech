---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)

![w:150](assets/logo-blanc.svg)

## TALISMAN Project
## Annual Meeting
## WPA
## 2024, 15 March

![w:100](assets/LOGO%20TALISMAN.png)

---
<!-- paginate: true -->
<!-- footer: Annual Meeting - ANR-TALISMAN - 3/15/24 CC:BY-NC-SA -->


# Personnes nouvellement impliquées dans TALISMAN

- **1 nov. 2023** : Recrutement en contrat doctoral d'Aurélie Huret (travail sur les déterminants de l'engagement analysables en CAC). Cf. sa présentation
- **Décembre 2023** : Recrutement post-doc de Carole Hanner (contrat MANIP), pour favoriser le lancement d'expérimentations dans les salles. Cf. le compte-rendu de Salomé
-  **Novembre 2023-Mai 2024** : Accueil d'Aurore Rèmes, stagiaire M2 MEEF-PIF, Univ. Poitiers 
	-  travail sur Intenses, l'outil d'observation humaine de l'engagement dans le supérieur, conçu par L. Lardy & Ph. Dessus
	-  travail sur un outil de formation professionnelle des enseignants les amenant à réfléchir à leur enseignement et l'engagement des étudiants
-  **Mars 2024** : Travail d'approche pour un projet de co-encadrement d'un doctorant de sciences de gestion : Étude qualitative de la prise en compte de l'éthique au sein de l'équipe de chercheurs, avec une dimension interculturelle (Fr/Asie)

---
# :fireworks::fireworks::fireworks::fireworks::fireworks::fireworks::fireworks:
**Livraison finale du DA.1 imminente**

---
# Plan Deliverable A.1 (DA.1)
Co-auteurs : Philippe Dessus, Tanja Petelin, Romain Laurent, Hassina El-Kechaï, Salomé Cojean

- 1  **Introduction**
- 2  **Legal issues** | RGPD, AI Act. Implications de ces règlementations sur la mise en œuvre de recherches impliquant les CAC
- 3  **Ethical issues** | Mobilisation de 2 cadres d'analyse des valeurs impliquées dans des systèmes techniques, le *Value sensitive design* (Friedman et al.) & les *Basic Individual values (Schwartz et al.)
- 4  **Questionnaires éthique** | Questionnaires VSD pour 1/ les partenaires de TALISMAN (*N* = 16) 2/ les parties prenantes (étudiants/enseignants)(*N* = 50), avec leurs premiers résultats

---
# Questionnaire partie 4 (1/2) (thèse de Romain Laurent)

- Questions sur la proximité des participants avec la CAC (s'ils s'estiment directement impliqués ou pas), les bénéfices/risques attribués à la CAC (6 de chaque max.)
- Analyse qualitative de ces risques pour en dégager des valeurs principales
- 14 catégories déterminées

---
# Questionnaire partie 4 (2/2)

|Centrées bénéfice | Centrées risques |
|------------------|------------------|
| Adaptation aux étudiants | Risque technologique
| Connaissance | Contrôle/surveillance
| Performance | Respect de la vie privée
| Adaptation au contexte | Autonomie
| Monitoring | Biais





---
# Livrable suivant : DA.2

- cf. présentation de Salomé Cojean
  
---
# Dernier livrable DA.3

- Recherche de collaborations avec collègues qui travaillent dans des directions proches (conception et test d'un “jeu sérieux“ pour amener des problématiques éthiques) 
- Deux pistes à creuser  (en plus de celle du doctorant SI déjà évoqué)
	- [Martin Ragot](https://www.researchgate.net/profile/Martin-Ragot), entreprise IRT b-com (contact : Salomé) : hip hip hip IA
	- [Ann-Louise Davidson](https://www.concordia.ca/faculty/ann-louise-davidson.html), Concordia univ., Canada, Jeu “[Ethical Pursuit](https://www.obvia.ca/ressources/la-quete-de-lia-ethique)”

---
# Travail sur Intenses (Aurore R., Laurent L., Philippe D. & Salomé C.)

- **IntEnsEs** “Interactions Enseignant-Étudiants dans le Supérieur”, outil d'observation des interactions enseignant-étudiants, fondé sur la théorie de l'autodétermination
- **3 dimensions** : Structuration des activités ; soutien de l'autonomie ; qualité des relations interpersonnelles
- Capture d'env. 2 h de cours de statistiques (de et par Laurent) + travail sur un autre cours
- Projet de conception d'une vidéo interactive (sous H5P ou autre) pour représenter les dimensions d'IntEnsEs et former les codeurs
- La validation de l'outil permettra, à terme, une utilisation dans TALISMAN, pour coder la qualité des interactions enseignant-étudiants

