---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)

![w:150](assets/logo-blanc.svg)

## TALISMAN Project
## Steering Group # 2
## WPA-LaRAC
## Philippe Dessus, resp. WPA
## 8 juillet 2024

![w:100](assets/LOGO%20TALISMAN.png)

---
<!-- paginate: true -->
<!-- footer: Projet ANR TALISMAN - Steering Group-2-8/07/2024 CC:BY-NC-SA -->


# Vue globale de l'avancement du WPA

- **DA.1** Livrable achevé le 9 avril 2024 (retard : 5 mois).
- **DA.2** Travail débuté, sous l'impulsion de Salomé (date prescrite de livraison : mai 2024, date réelle : dernier trimestre 2024).
- **DA.3** Pistes toujours au point mort, pour l'instant. Un plan pour avancer sur ce livrable sera fait à la rentrée.

---
# Autres contributions du LaRAC

- Contribution au D1.1, en cours de finalisation (dernières révisions envoyées à TECHNE le 19 juin 2024)
- Stage d'A. Rèmes (MEEF-PIF, univ. Poitiers)
  - Codage d'un TD de statistiques
  - Construction d'un cours Moodle pour présenter IntEnSES, un outil d'observation des relations enseignant-étudiants dans le supérieur
  - Présentation à la DAPI, UGA, le 17 juin dernier. Collaborations à venir avec la DAPI pour tester l'outil, à la rentrée

---
# Point avancement Livrable DA.2 (Salomé Cojean) (1/3)

- Acceptabilité des CAC. L’acceptabilité d’un système (avant usage)  dépend de nombreux facteurs. Selon les modèles les plus connus et utilisés (e.g., TAM, UTAUT), l’intention d’usage dépend principalement de facteurs “pragmatiques” (e.g., utilité perçue, facilité d’usage perçue)
- Des travaux récents montrent l’importance de prendre en compte également d’autres facteurs dits “affectivo-motivationnels”. Le jugement de l’IA en général (e.g., attitude envers l’IA, facteurs éthiques) pourrait influencer l’intention à utiliser un outil en particulier

---
# Point avancement Livrable DA.2 (Salomé Cojean) (2/3)

- **Étude 1 (réalisée)** : Effet de l’expertise en IA sur l’acceptabilité des outils pédagogiques avec IA
- **Hypothèses** : Les personnes qui ne connaissent pas le fonctionnement de l’IA ou qui en ont des préconceptions erronées auront un jugement plus négatif des outils IA 
- **Public** : Étudiants en master MEEF (1er + 2nd degré)
- **Méthode** : Questionnaire préalable pour répartir les participants en deux catégories (*median split*) : experts vs. novices
- **VD** : intention d’usage, utilité perçue, confiance 
- **Résultats** : Effet de l’expertise déclarée (davantage que de l’expertise évaluée par le questionnaire), jugement plus positif pour les experts déclarés

---
# Point avancement Livrable DA.2 (3/3 SC avec Carole Hanner)

- **Étude 2 (en préparation)** : Effet de la présentation de l’outil (TL) sur son acceptabilité
- **Hypothèses** : L’utilisation du terme IA est associée à des jugements négatifs envers l’outil. Le jugement de l’outil peut être rendu plus positif avec des démonstrations concrètes
- **Méthode** : Avant un premier cours dans TL, les étudiants se verront présenter le TL, selon 4 conditions (avec le terme IA utilisé ou non, avec démonstration ou non de l’analyse des résultats anonymement) 
- **VD** : intention d’usage, utilité perçue (pour l’enseignant + pour les étudiants), éthique (confiance + contrôle + *privacy*).

---
# Point avancement DA.2

- Voir à ajouter le travail sur les persona (TECHNE) au DA.2 ? À discuter entre TECHNE et LaRAC à la rentrée de septembre.
  
---
# Dernier livrable DA.3

- Recherche de collaborations avec collègues qui travaillent dans des directions proches (conception et test d'un “jeu sérieux“ pour amener des problématiques éthiques) 
- Deux pistes à creuser (contact avec ALD toujours en cours, contact avec MR reporté)
    - [Ann-Louise Davidson](https://www.concordia.ca/faculty/ann-louise-davidson.html), Concordia univ., Canada, Jeu “[Ethical Pursuit](https://www.obvia.ca/ressources/la-quete-de-lia-ethique)”
	- [Martin Ragot](https://www.researchgate.net/profile/Martin-Ragot), entreprise IRT b-com (contact : Salomé) : hip hip hip IA
	

---
# Travail de thèse de Romain Laurent (valeurs CAC)

- Finalisation de l'analyse des questionnaires bénéfices/risques de ≠ parties prenantes, menant à déterminer des valeurs précises, *via* intercodage qualitatif.
- Projet de lancer un questionnaire à très large échelle à partir de ces valeurs
- Revue de la question sur les ≠ données récupérées dans les CAC

Ces résultats pourront alimenter un ou plusieurs Livrables à venir (en discussion)

---
# Travail de thèse d'Aurélie Huret (observation engagement)

- Première étude (hors TL) en cours pour étudier l'engagement (comportemental, émotionnel, cognitif) des étudiants, triangulation de mesures :
  - mesure externe d'étudiants (observatrice en fond de classe)
  - mesure d'opinion étudiant
  - mesure d'opinion des enseignants
- Observation d'une dizaine de cours de matières et organisations différentes
- Pourra alimenter possiblement le D1.2

---
# Autres pistes possibles (en cours de travail)

- Utiliser TL pour étudier la différence d'engagement de l'enseignant et des étudiants dans des cours de médecine (anatomie) selon deux supports : schémas à la craie sur tableau vs. sur tablette (avec la collaboration de P.-Y. Rabattu, MCF-PH, UGA, membre associé du LaRAC)
- Collaboration possible (plutôt sur les aspects matériels et éthiques) avec Laurent Jeannin, Lab. BONHEURS (CY Cergy), qui monte deux salles expérimentales. 1re réunion de discussion à venir très prochainement


---
# Publications récentes

- Hoareau, É., Dessus, P., & Amer-Yahia, S. (2024, May 27-29). AIED ethics, a managerial perspective. In *29e Conférence de l'Association Information et Management*. La-Grande-Motte. 