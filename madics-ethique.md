---
marp: true
paginate: true
autoscale: true
theme: aqua
---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)


# Ethique en AIED

Philippe Dessus
LaRAC, Univ. Grenoble Alpes

## Atelier EducAction, MaDICS-CNRS du 30 janvier 2023

---
<!-- paginate: true -->
<!-- footer: Ph. Dessus | EducAction-MaDICS | 30/01/23 | CC:BY-NC-SA -->

# But de la sous-partie “Éthique”

- Prendre connaissance d'un cadre d'analyse des implications éthiques de votre système AIED
- L'appliquer à votre système
- En rendre compte pour l'atelier MaDICS

:warning: Cette présentation résume les grandes lignes des informations du doc. sur l'éthique, disponible dans le dossier partagé du Cloud “1. Ethique”.

---
# Flux d'information - Conception et test d'un AIED

![w:1100](images/cycle-buck.jpg)

---
# Les règles éthiques

- de très nombreuses règles éthiques pour l'AIED existent, mais :
  - elles n'insistent pas assez sur l'équilibre bénéfices/risques des règles (e.g., concevoir un système contrôlant le rend aussi surveillant)
  - elles sont plus orientées données AI qu'ED (éducation)
  - elles ne tiennent pas compte des étapes du processus de la recherche, où ces règles peuvent être diversement appliquées


---
# Le processus de la recherche (Elliott 2022)

![w:1100](images/phases-sci.jpg)

---
# But de la démarche (voir doc. p. 9)

1. Identifier (à partir de la fiche descriptive) une **sous-partie de votre système** AIED signifiante, que vous voulez analyser, un contexte d'utilisation et une valeur principale
2. Identifier les **valeurs principales** (AI *et* ED) que vous voulez promouvoir dans votre système AIED 
3. Identifier les **utilisateurs directs ou indirects** (voir cycle ci-dessus)
4. Identifier les **bénéfices/risques** de l'application de ces valeurs
5. Déterminer quelle.s **facette.s éducative.s** votre système promeut
6. Choisir l'une des quatre **phases de la recherche** qui vous paraît la plus pertinente dans votre travail et, en prenant la sous-partie de votre système pour exemple, répondre aux questions qui vous sembleront pertinentes.


---
# Un exemple de démarche : Le *Teaching Lab* (1/4)

1. Identifier une sous-partie du système
  
> Nous sélectionnons la phase de capture et analyse vidéo des micro-expressions faciales et de la posture des étudiants.

2. Identifier les utilisateurs directs et indirects
  
> Les utilisateurs directs sont les **chercheurs** ; les utilisateurs indirects sont les **enseignants** et les **étudiants** puisqu’ils n’interagissent pas directement avec le système. Notons toutefois qu’une interaction minimale est prévue : celle où un appui sur un bouton sur un mur de la salle arrête la capture en cours et supprime le fichier vidéo de la séance.

---
# Un exemple de démarche : Le *Teaching Lab* (2/4)

3. Bénéfices et inconvénients, par groupes d'utilisateurs et valeurs

> Les **chercheurs** se servent des captures et analyses pour comprendre les liens entre changements d’expression et engagement des étudiants. Les **enseignants**, après séance de cours, peuvent tirer parti des analyses précédentes pour analyser leur pratique d’enseignement et déterminer les plus efficaces, au détriment d’une surveillance sur la durée du cours. Les **étudiants** peuvent, eux aussi, avoir un avantage à comprendre ce qui les engage et désengagent dans un cours, là aussi au détriment d’une surveillance sur la durée du cours.

---
# Un exemple de démarche : Le *Teaching Lab* (3/4)

4. Volet éducation

> La facette la plus importante est celle de promouvoir les vérités du monde réel, puisque les enseignants se voient confrontés à des analyses fines des tenants et aboutissants de leurs choix pédagogiques

---
# Un exemple de démarche : Le *Teaching Lab* (4/4)

5. Réponse aux questions éthiques dans le processus de recherche
   
> Nous pouvons réfléchir à l’étape “Faire de la recherche”. Le cadre théorique utilisé est celui de l’**engagement** (comportemental et émotionnel) des étudiants. Le but est d’utiliser un système d’IA pour étiqueter automatiquement les comportements et changements émotionnels de haut ou bas niveau, et pouvoir par cela procurer une évaluation des conséquences de telle ou telle méthode pédagogique. À cet effet, il est donc nécessaire de récupérer des informations sur la posture (direction du buste, de la tête) et sur les micro-expressions faciales. Cela ne paraît pas être un prélèvement de données excessif par rapport au but. 
Les analyses faites à ces propos ne seront jamais communiquées aux utilisateurs en direct, afin d’éviter toute interférence ou surveillance, mais *a posteriori*, dans des temps de debriefing. Il est encore trop tôt pour élaborer des conclusions fermes à propos de ce projet, puisque nous n’avons pas commencé à en récolter des données.


---
# Discussion

- Que pensez-vous de ce cadre ? Facilité d'utilisation ? Intérêt pour vos recherches ? Points manquants ?

- Merci de faire remonter aux organisateurs tout élément du cadre ou de la démarche 1/ peu clair ; 2/ non applicable à votre système, pour réfléchir à une adaptation

- Si vous êtes partant.es pour contribuer, envoyez svp aux organisateur.es (Sihem, Émilie et Philippe) le résultat de votre réflexion (les 6 points)


---
# Remerciements

- Des questions ? Des commentaires ?

- Merci à Sihem Amer Yahia, Émilie Hoareau, Salomé Cojean, & Romain Laurent pour leur input dans la réalisation de ce travail