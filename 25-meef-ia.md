---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
  
# Intégrité académique et IA générative

## Réunion des resp. d'UE SD - Inspé UGA 
## Philippe Dessus - LaRAC, LIG, Inspé, UGA

![w:200](images/logo-espe-uga.png)
 
---
<!-- page_number: true -->
<!-- paginate: true -->
<!-- footer: IA et GenIA – Ph. Dessus  - Réunion resp. UE MEEF-Inspé UGA - 18/03/2025 - CC:BY-NC-SA -->

# :zero: Informations 

- Présentation disponible à partir de ma page personnelle ([pdessus.fr](https://pdessus.fr)) et ici : XXX

---
# :zero: But de la présentation

- Donner quelques éléments sur l'usage des outils d'intelligence artificielle générative à l'université
- Approche considérant toutes les parties prenantes et pas seulement les étudiant.es
- Approche mettant en avant l'intégrité académique

---
# 0. Bref aperçu de son fonctionnement

![w:400](images/llm.jpg)


:book: Seminck (2025) 

---
# 0. Multimodalité et parallélisme

A FINIR

---
# :one: 1. Quelques définitions

- Intégrité académique 
> “Les universités considèrent qu’il est essentiel que les étudiants et l’ensemble du personnel agissent de manière honnête et assument la responsabilité de leurs actes et de chaque partie de leur travail”. (Universities Australia, 2017)

---
# :two: 0. Format du questionnement

:question: On pose une question relative à l'IA de la GenIA
:now: On explique ce qu'il en est actuellement
:magic_wand: On dit ce que ça pourrait être, en mieux

---
# :two: 1. Intégrité académique comme valeur principale

:question: L'intégrité académique est-elle la valeur principale des parties prenantes du milieu universitaire ?
:now: Oui, supposé par défaut pour **toutes** les parties prenantes 
:magic_wand: On pourrait renforcer l'intégrité académique en informant mieux les parties prenantes (qui seront d'autant moins enclines à l'enfreindre que leurs pairs la respectent, Hendy & Montargot, 2019)

---
# :two: 2. Le suivi et l'évaluation de travaux universitaires

:question: Quelle place accorder au suivi et à l'évaluation de documents universitaires ? 
:now: Si les enseignant.es sont surchargé.es de tâches, ils ne pourront accorder suffisamment de temps à évaluer les travaux des étudiant.es, ce qui ne va pas favoriser l'intégrité académique
:magic_wand: On pourrait renforcer l'intégrité académique en chargeant moins les enseignant.es. On peut aussi favoriser l'oral ou l'écrit “sur table“ plutôt que l'écrit à la maison (Khedidja, 2018)

---
# :two: 3. Quelle validité des productions de la GenIA ?

- :question: Dans quelle mesure l'output de la GenIA est exempt d'erreurs (relatives aux connaissances, aux procédures) ?
- :arrow_down: **Performance loin d'être exempte d'erreurs**. Peut être utilisée comme “étudiant-pair”, plus difficilement comme “enseignant”. Difficile de dire (comme le font Han et al. 2024) qu'on peut se servir de ces erreurs pour exercer l'esprit critique des étudiants
- :magic_wand: Si sa fiabilité augmentait, il serait possible de l'utiliser pour enseigner ou tuteurer (Netland et al., 2025)

---
# :two: 4. Quel autorat donner aux productions de la GenIA ?

- :question: Qui sont les auteur.es des productions de la GenIA ? Elle seule ? Avec les étudiant.es ?
- :arrow_down: On peut prescrire l'autorat, sans certitude qu'il soit respecté (car la GenIA devient omniprésente, ce qui augmente les chances des usages non intentionnels), sauf si on contraint les conditions de production (cf. question 2)
- :magic_wand: Demander aux parties prenantes de déclarer *a priori* leur usage de GenIA, en gardant à l'esprit que les usages non licites risquent de ne pas être déclarés

---
# :two: 5. Quelle détectabilité de la GenIA ?

- :question: Dans quelle mesure la production de la GenIA est détectable (dans les documents synthétiques ou mixtes) ?
- :arrow_down: À ce jour, elle l'est difficilement (peu de faux positifs et de vrais positifs)(Dessus et Seyve, 2024). Il est donc très délicat de sanctionner des usages, qui ne seront que *supposés* (sauf si aveu des parties prenantes)
- :magic_wand: Si (et seulement si) sa détectabilité était meilleure, il serait possible d'en contrôler plus strictement l'usage

---
# :two: 6. Quelle (in)formation à la GenIA ?

- :question: Quelle formation ou information à la GenIA donner aux parties prenantes ?
- :arrow_down: GenIA peu présente dans les règlements des études des universités françaises, ou alors de manière peu tranchée (pas de bannissement absolu, mais utilisations)
- :magic_wand: Insister sur l'intégrité académique, démagifier la GenIA (Lupetti & Murray-Rust, 2024), la rendre souveraine et libérée des enjeux commerciaux

---
# Merci de votre attention ! 
Des :question: :question: :question:

---
# Remerciements

Cette présentation a bénéficié des réflexions de Sébastien Jolivet, que je remercie.

---
# Références

- Dessus, P. & Seyve, D. (2024). [La détection de l'utilisation de robots conversationnels en contexte universitaire : Le cas de Compilatio Magister+](https://hal.univ-grenoble-alpes.fr/hal-04578682v1/document). Pre-print HAL.
- Han, Z., Battaglia, F., & Terlecky, S. R. (2024). Transforming challenges into opportunities: Leveraging ChatGPT's limitations for active learning and prompt engineering skill. *The Innovation Medicine, 2*(2). https://doi.org/10.59717/j.xinn-med.2024.100065 
- Hendy, N. T., & Montargot, N. (2019). Understanding Academic dishonesty among business school students in France using the theory of planned behavior. _The International Journal of Management Education_,_ 17_(1), 85-93. https://doi.org/10.1016/j.ijme.2018.12.003 
- Khedidja, Z. (2018, 3 septembre). Un écrivain public strasbourgeois brise le tabou de la fraude aux thèses à l’Université. https://www.rue89strasbourg.com/ecrivain-public-strasbourg-fraude-theses-universite-140693
- Lupetti, M. L., & Murray-Rust, D. (2024). (Un)making AI Magic: A Design Taxonomy. Proceedings of the CHI Conference on Human Factors in Computing Systems. https://doi.org/10.1145/3613904.3641954
- Netland, T., von Dzengelevski, O., Tesch, K., & Kwasnitschka, D. (2025). Comparing human-made and AI-generated teaching videos: An experimental study on learning effects. *Computers & Education, 224*. https://doi.org/10.1016/j.compedu.2024.105164 
- Seminck, O. (2025). [Les grands modèles de langues pour les nuls](https://mate-shs.cnrs.fr/actions/tutomate/tuto64_llm_seminck/). Conférence MATE-SHS.
- Universities Australia. (2017). [Academic integrity best practices](https://universitiesaustralia.edu.au/wp-content/uploads/2019/06/UA-Academic-Integrity-Best-Practice-Principles.pdf). 

