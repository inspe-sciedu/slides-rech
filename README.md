# Présentations recherche

- Dessus, P. & Seyve, D. (2024, 29 nov.). Pour une littératie de l'IA générative chez les enseignants et étudiants. Grenoble : Webinaire Minalogic-MIAI. [[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides-rech/24-EFELIA-webinaire.pdf)][[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides-rech/24-EFELIA-webinaire.html)]

- Charroud, C., Dessus, P. & Osete, L. (2024, 14 nov.). Organisation et mise à jour de la base de documents de cours sur le numérique et les sciences de l'éducation. Grenoble : Univ. Grenoble Alpes, Inspé, séminaire sur les transformations d'U.E. [[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides-rech/24-transf-UE-sphinx.pdf)][[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides-rech/24-transf-UE-sphinx.html)]

- Dessus, P. (2024, 6 mars). Usages pédagogiques des systèmes générateurs de texte. Séminaire EFELIA-MIAI. Univ. Grenoble Alpes, LIG. [[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides-rech/24-Efelia.pdf)] [[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-]alpes.fr/slides-rech/24-Efelia.html)]

- Vadcard, L. & Dessus, P. (2023, 23  nov.). Deux services Gricad pour aider la recherche et l’enseignement : Transcription automatique et documents de cours collaboratifs. Séminaire du LaRAC, UGA. [[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides-rech/23-outils-gricad-larac.pdf)] [[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-]alpes.fr/slides-rech/23-outils-gricad-larac.html)]

- Dessus, P. (2023, 13 juillet). What can a Context-aware Classroom do for you? Séminaire M-PSI, LIG, Univ. Grenoble Alpes. [[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides-rech/23-pres-CAC-LIG.pdf)] [[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-]alpes.fr/slides-rech/23-pres-CAC-LIG.html)]
  
- Dessus, P. & Ménard, F. (2023, 29 juin). Libérez les ressources d'enseignement ! Des manuels et présentations libres et ouvertes avec Gitlab à l'université. Présentation au GT “Données” de la MITI CNRS. Paris. [[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides-rech/23-donnee-miti.pdf)] [[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides-rech/23-donnee-miti.html)]

- Dessus, P. (2023, 21 mars). Observer et analyser la qualité des relations enseignant-élèves. Webinaire C.A.R.E.C., Rectorat de l'académie de Grenoble. [[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides-rech/23-carec.pdf)] [[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides-rech/23-carec.html)]
